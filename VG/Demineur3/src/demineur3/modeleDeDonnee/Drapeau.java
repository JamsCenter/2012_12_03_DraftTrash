/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.modeleDeDonnee;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;

/**
 *
 * @author java06
 */
public class Drapeau extends Element{

    
    private static int nombreDeDrapeau=0;
    
    //private  final Image img = new javax.swing.ImageIcon(getClass().getResource("/demineur3/affichage/ressources/drapeau.jpg")).getImage() ;
    {
    nombreDeDrapeau++;
    nom = "Drapeau"+nombreDeDrapeau;
    imageParDefaul = new javax.swing.ImageIcon(getClass().getResource("/demineur3/affichage/ressources/drapeau.png")).getImage() ;
    dimension = new Dimension(imageParDefaul.getWidth(null),imageParDefaul.getHeight(null));
    }
    
    public Drapeau(String nom, Point position,Dimension dimension,Image image) {
        super(nom, position,dimension, image);
    }

    public Drapeau(Point position) {
        super( position);
    }

    
    
    
}

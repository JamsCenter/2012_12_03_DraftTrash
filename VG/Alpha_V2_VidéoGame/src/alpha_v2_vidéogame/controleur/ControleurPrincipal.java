package alpha_v2_vidéogame.controleur;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import alpha_v2_videogame.moteur.TimerCoeur;
import alpha_v2_vidéogame.Fenetre;
import alpha_v2_vidéogame.JLayoutPanel;
import alpha_v2_vidéogame.mécanisme.EtatDesTouchesUtilisateur;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import alpha_v2_vidéogame.mécanisme.SérieDeMéthodeStatic;

/**
 *Ce controleur crée les controleurs du jeu et les synchoronises
 * @author Eloi & Fab
 */
 public  class ControleurPrincipal extends Controleur {

    private Fenetre fenetre;
    private JLayoutPanel[] layoutsApplication;
    private TimerCoeur timer;

    private GameData gameDataControler;
    private ControleurGraphique graphicsControler;
    private ControleurClavier keyBoardControler;
    private ControleurDéplacement movesControler;
    private ControleurScrolling scrollingControler;
    private ControleurUtilisateur userControler;
    private ObjectInScreen gestionnaireDesElementsEnJeu;



     {
        gestionnaireDesElementsEnJeu = new ObjectInScreen();

     }
    public ControleurPrincipal( Fenetre fenetre) {
        super();
        fenetre.createBufferStrategy(4);
        gameDataControler = new GameData(this);
        movesControler = new ControleurDéplacement(this,gestionnaireDesElementsEnJeu);
        graphicsControler = new ControleurGraphique(this,gestionnaireDesElementsEnJeu,fenetre);
        scrollingControler = new ControleurScrolling(this,gestionnaireDesElementsEnJeu,gameDataControler);
        userControler = new ControleurUtilisateur(this);
        //userControleur.setMovableElement();
        userControler.setAvailable(true);

        this.fenetre = fenetre;
        keyBoardControler = new ControleurClavier(this,fenetre);
        Controleur.getFenetreAideProgrammeur().addKeyListener(keyBoardControler);


        this.timer = new TimerCoeur(this,15,30,250);
        SérieDeMéthodeStatic.setTimer(timer);
        play();



        userControler.setPersonnageControler(gameDataControler.getMainCharacter());
        userControler.setAvailable(true);
        // à déplacer plus tard

        scrollingControler.analyseWhatToUse();
        //PaintElement.paintThisImageIn(fenetre.getJPanelBackGround(), gameDataControler.getBackground());

        // end à déplacer plus tard
 
    }

    void signalDuClavier(KeyEvent e,EtatDesTouchesUtilisateur etat) {

        if(userControler.isAvailable())
        {

            userControler.signalDuClavier(e,etat);
        }


    }

    public void signalCalculGraphique(ActionEvent e) {
        graphicsControler.signalCalcul(e);
    }

    public void signalCalculPhysique(ActionEvent e) {
        movesControler.signalCalcul(e);
    }

    public void signalCalculIA(ActionEvent e) {
    }






    
     public final void play (){
       timer.play();
       userControler.setAvailable(true);
       notifyChange("play");

    }
    public void pause (){
       timer.pause();
       userControler.setAvailable(false);
       notifyChange("pause");
    }

    public GameData getGameDataControler() {
        return gameDataControler;
    }

    public ObjectInScreen getGestionnaireDesElementsEnJeu() {
        return gestionnaireDesElementsEnJeu;
    }

    public ControleurGraphique getGraphicsControler() {
        return graphicsControler;
    }

    public ControleurDéplacement getMovesControler() {
        return movesControler;
    }

    public ControleurScrolling getScrollingControler() {
        return scrollingControler;
    }

    public ControleurUtilisateur getUserControler() {
        return userControler;
    }



  
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.donnees;


import java.awt.Point;
import java.util.Observable;

/**
 *
 * @author christine
 */
public class ModèleInterface extends Observable{
    
    private int widthGameWindows=800;
    private int heightGameWindows=600;

    private boolean KeyBoardUp = false;
    private boolean KeyBoardLeft = false;
    private boolean KeyBoardDown = false;
    private boolean KeyBoardRight = false;
    
    
    private Point MousePositionActuelle = null;
    
    private boolean MouseBUTTON1 = false;
    private Point MousePositionBUTTON1Pressed = null;
    private Point MousePositionBUTTON1Released = null;   
    private int nombreEventDepuisBUTTON1Pressed =0;
    
    private boolean MouseBUTTON3 = false;
    private Point MousePositionBUTTON3Pressed = null;
    private Point MousePositionBUTTON3Released = null;
    private int nombreEventDepuisBUTTON3Pressed =0;
    
    {
    
    
    
    }
    
   
    public boolean isKeyBoardDown() {
        return KeyBoardDown;
    }

    public void setKeyBoardDown(boolean KeyBoardDown) {
        this.KeyBoardDown = KeyBoardDown;
        notifierAuxObserveurs();
    }

    public boolean isKeyBoardLeft() {
        return KeyBoardLeft;
    }

    public void setKeyBoardLeft(boolean KeyBoardLeft) {
        this.KeyBoardLeft = KeyBoardLeft;
        notifierAuxObserveurs();
    }

    

    public boolean isKeyBoardUp() {
        return KeyBoardUp;
    }

    public void setKeyBoardUp(boolean KeyBoardUp) {
        this.KeyBoardUp = KeyBoardUp;
        notifierAuxObserveurs();
    }

    public boolean isMouseBUTTON1() {
        return MouseBUTTON1;
    }

    public void setMouseBUTTON1(boolean MouseBUTTON1) {
        this.MouseBUTTON1 = MouseBUTTON1;
    }

    public boolean isMouseBUTTON3() {
        return MouseBUTTON3;
    }

    public void setMouseBUTTON3(boolean MouseBUTTON3) {
        this.MouseBUTTON3 = MouseBUTTON3;
    }


    public boolean isKeyBoardRight() {
        return KeyBoardRight;
    }

    public void setKeyBoardRight(boolean KeyBoardRight) {
        this.KeyBoardRight = KeyBoardRight;
        notifierAuxObserveurs();
    }

    public Point getMousePositionActuelle() {
        return MousePositionActuelle;
    }

    public void setMousePositionActuelle(Point MousePositionActuelle) {
        this.MousePositionActuelle = MousePositionActuelle;
    }

    public Point getMousePositionBUTTON1Pressed() {

        return MousePositionBUTTON1Pressed;
    }

    public void setMousePositionBUTTON1Pressed(Point MousePositionBUTTON1Pressed) {
        this.MousePositionBUTTON1Pressed = MousePositionBUTTON1Pressed;
        
        nombreEventDepuisBUTTON1Pressed++;
    }

    public Point getMousePositionBUTTON1Released() {
        return MousePositionBUTTON1Released;
    }

    public void setMousePositionBUTTON1Released(Point MousePositionBUTTON1Released) {
        this.MousePositionBUTTON1Released = MousePositionBUTTON1Released;
        nombreEventDepuisBUTTON1Pressed=0;
    }

    public Point getMousePositionBUTTON3Pressed() {
        return MousePositionBUTTON3Pressed;
    }

    public void setMousePositionBUTTON3Pressed(Point MousePositionBUTTON3Pressed) {
        this.MousePositionBUTTON3Pressed = MousePositionBUTTON3Pressed;
        nombreEventDepuisBUTTON3Pressed++;
    }

    public Point getMousePositionBUTTON3Released() {
        
        return MousePositionBUTTON3Released;
        
    }

    public void setMousePositionBUTTON3Released(Point MousePositionBUTTON3Released) {
        this.MousePositionBUTTON3Released = MousePositionBUTTON3Released;
        nombreEventDepuisBUTTON3Pressed=0;
    }

    public int getHeightGameWindows() {
        return heightGameWindows;
    }

    public void setHeightGameWindows(int heightGameWindows) {
        this.heightGameWindows = heightGameWindows;
    }

    public int getWidthGameWindows() {
        return widthGameWindows;
    }

    public void setWidthGameWindows(int widthGameWindows) {
        this.widthGameWindows = widthGameWindows;
    }
    
    
    public boolean isNotPressedYetBUTT3()
    {
     if (nombreEventDepuisBUTTON3Pressed==0)return true;
     return false;
    }
    public boolean isNotPressedYetBUTT1()
    {
     if (nombreEventDepuisBUTTON1Pressed==0)return true;
     return false;
    }
    

    @Override
    public String toString() {
        
        String tmp ="#####  Key\n";
        
        if (KeyBoardUp) tmp+="  Up:"+KeyBoardUp;
        if (KeyBoardLeft) tmp+="  Left:"+KeyBoardLeft;
        if (KeyBoardDown) tmp+="  Down:"+KeyBoardDown;
        if (KeyBoardRight) tmp+="  Right:"+KeyBoardRight;
        
        tmp +="#####  \nMouse\n";
        
        if (MousePositionActuelle!=null) tmp+="\tPosition souris: ("+MousePositionActuelle.x+","+MousePositionActuelle.y+")\n";
        if (MouseBUTTON1 && MousePositionBUTTON1Pressed!=null) tmp+="\tClick gauche: ("+MousePositionBUTTON1Pressed.x+","+MousePositionBUTTON1Pressed.y+")\n";
        if (MouseBUTTON3 && MousePositionBUTTON3Pressed!=null) tmp+="\tClick droit: ("+MousePositionBUTTON3Pressed.x+","+MousePositionBUTTON3Pressed.y+")\n";
        
        
        return tmp;
        
    }

    public void notifierAuxObserveurs()
    {
    setChanged();
    notifyObservers();
    
    
    }
    
   

}

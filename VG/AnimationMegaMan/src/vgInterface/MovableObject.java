package vgInterface;

import java.awt.Point;

import ClassDeBase.Move;


public interface MovableObject {

	public Point getThePosition();
	public Move getTheMove();
	

}

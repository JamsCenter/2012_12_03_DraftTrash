/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alpha_v2_vidéogame.mécanisme;

/**
 *
 * @author Eloi
 */
public class FloatPoint  {

    public  float x;
    public  float y;

    public FloatPoint(){x=0;y=0;}
    public FloatPoint(float x, float y){this.x=x;this.y=y;}

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }



}

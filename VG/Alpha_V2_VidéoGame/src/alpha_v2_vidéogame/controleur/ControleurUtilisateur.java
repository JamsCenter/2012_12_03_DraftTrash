/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alpha_v2_vidéogame.controleur;

import alpha_v2_videogame.modèle.ElementMovible;
import alpha_v2_videogame.modèle.personnage.Personnage;
import alpha_v2_vidéogame.AideAuxProgrammeurs;
import alpha_v2_vidéogame.mécanisme.EtatDesTouchesUtilisateur;
import java.awt.event.KeyEvent;

/**
 *
 * @author Eloi
 */
public class ControleurUtilisateur extends Controleur {

    /** permet de bloquer le controle du héro par exemple pour une cinématique ... */
    private boolean available;
    /** désigne le personnage que controle ce controleur */
    private ElementMovible elementControled;

    {
        available = false;
    }

    public ControleurUtilisateur(ControleurPrincipal aThis) {
        super();
    }

    /** réagit si le controleur possède un personnage à déplacer et si il est en état disponible */
    boolean isAvailable() {
        if (!available) {
            return false;
        }
        if (elementControled == null) {
            return false;
        }
        return true;
    }

    void setAvailable(boolean b) {
        available = b;
        if (b) {
            notifyChange("Available");
        } else {
            notifyChange("Not Available");
        }
    }

    void signalDuClavier(KeyEvent e, EtatDesTouchesUtilisateur etat) {
  
        if (isAvailable()) {


            if (etat.isAllArrowReleased()) {
                elementControled.setMoving(false);
            } else if (!etat.isAllArrowReleased()) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_RIGHT:
                        elementControled.setVitesseHorizontal(100);
                        break;
                    case KeyEvent.VK_LEFT:

                        elementControled.setVitesseHorizontal(-100);
                        break;
                    case KeyEvent.VK_UP:

                        elementControled.setVitesseVertical(-100);
                        break;
                    case KeyEvent.VK_DOWN:

                        elementControled.setVitesseVertical(+100);
                        break;
                }
            }
             switch (e.getKeyCode()) {
                   
                    case KeyEvent.VK_SPACE:

                        elementControled.jump(-400);
                        break;
                    case 82:

                        elementControled.setXY(200, 200);
                        break;
                    case 70:

                       elementControled.touchTheGround();
                        break;
                }
            notifyChange(elementControled);
            notifyChange(elementControled.getEtatDeplacement());
        }
    }

    public void setPersonnageControler(Personnage personnageControler) {
        this.elementControled = personnageControler;
        notifyChange(personnageControler);
    }
}

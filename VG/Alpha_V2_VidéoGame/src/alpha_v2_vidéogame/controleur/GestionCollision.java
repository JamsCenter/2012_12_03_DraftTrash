/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alpha_v2_vidéogame.controleur;

import alpha_2v_videogame.modèle.carte.Map;
import alpha_2v_videogame.modèle.carte.Tuile;
import alpha_v2_videogame.modèle.ElementMovible;
import alpha_v2_videogame.modèle.personnage.Personnage;
import alpha_v2_vidéogame.mécanisme.FloatPoint;
import java.util.ArrayList;

/**
 *
 * @author Eloi
 */
public class GestionCollision {

    public GestionCollision(ControleurPrincipal controleurPrincipal) {
        
    }

      public FloatPoint gestionDeLaCollision(Map map, ElementMovible e,FloatPoint pt) {
        // si une carte est chargée
        if (map != null) {

         
                boolean collisionAvecSolide = false;
                ArrayList<Tuile> tuilleEnCollision;
                // si il y a des collisions
                if ((tuilleEnCollision = map.getAllTuilesTouch(e, pt)) != null) {
                    // ajouter les tuiles à la liste des objets à rafraichir avec un évènement d'animation
                    for (Tuile tmpT : tuilleEnCollision) {
                        System.out.println(tmpT);
                        tmpT.touchée(e);
                        if (tmpT.isSolide()) {
                            collisionAvecSolide = true;
                        }

                    }

                    if (!collisionAvecSolide) {
                        return pt;
                    } else {

                        e.inverseVitesseHV();
                        //e.getEtatDeplacement().setMoving(false);
                        e.getEtatDeplacement().setFalling(false);
                        // si collision travail de replacement
                        System.out.println("Collision");
                        return e.getPosition();
                    }
                }


         



            //else if(false /*si rencontre un bloc*/) {/* stopper*/}
            //else if(false /*si du vide en dessou*/) {/* appliquer la gravité*/}
        } //si pas de carte, je place le perso à une position fixe
   

return e.getPosition();


    }

     private boolean isThereSolideInThis(ArrayList<Tuile> tuilleEnCollision) {
        boolean collisionSolid=false;
        for (Tuile tmpT : tuilleEnCollision) {
            if (tmpT.isSolide()) {
                collisionSolid = true;
            }
        }
        return collisionSolid;

}

}

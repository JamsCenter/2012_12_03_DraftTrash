package vgqd.games.tuxdominationeditor.swing.gui;

import java.awt.Color;

import javax.swing.JEditorPane;

public class XmlPanel extends JEditorPane {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the panel.
	 */
	public XmlPanel() {
		this.setEnabled(false);
		this.setBackground(Color.black);
		this.setForeground(Color.green);
		this.setText("hello");
		this.setVisible(true);
	}

}

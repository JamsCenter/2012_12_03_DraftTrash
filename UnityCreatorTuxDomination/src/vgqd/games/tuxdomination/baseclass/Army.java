package vgqd.games.tuxdomination.baseclass;

import java.util.Arrays;

public class Army {
	
	private String name;
	private Unity [] unities= new Unity[]{};
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Unity[] getUnities() {
		return unities;
	}
	public void setUnities(Unity[] unities) {
		this.unities = unities;
	}
	@Override
	public String toString() {
		return "Army [name=" + name + ", unities=" + Arrays.toString(unities)
				+ "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + Arrays.hashCode(unities);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Army other = (Army) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (!Arrays.equals(unities, other.unities))
			return false;
		return true;
	}
	

}

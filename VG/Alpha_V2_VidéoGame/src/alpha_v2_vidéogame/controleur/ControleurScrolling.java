/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alpha_v2_vidéogame.controleur;

import alpha_v2_videogame.modèle.personnage.Personnage;
import alpha_v2_vidéogame.modèle.ConstantesDuJeu;

/**
 *Le role de ce controleur est de mettre à jours les éléments à prendre en compte et à dessinner
 * à chaque fois qu'on lui signale un mouvement.
 * Ou aux mieux enticiper les éléments à dessiner.
 * @author Eloi
 */
public class ControleurScrolling  extends Controleur implements ConstantesDuJeu {

    private ControleurPrincipal controleurPrincipal;
    private ObjectInScreen gestionnaireDesElementsEnJeu;
    private GameData  gameData;
    public ControleurScrolling(ControleurPrincipal aThis, ObjectInScreen gestionnaireDesElementsEnJeu, GameData gameData) {
    super();
    this.controleurPrincipal = aThis;
    this.gestionnaireDesElementsEnJeu = gestionnaireDesElementsEnJeu;
    this.gameData = gameData;
    }

    /**
     * Devra faire l'analyse des éléments à afficher qui appartienne au scrolling
     */
    public void analyseWhatToUse()
    {
    // en attendant de coder cela, je créer des instanciations.

        if (gameData!=null)
        {

            for(Personnage tmp : gameData.getPersonnages())
                gestionnaireDesElementsEnJeu.ajouterObject(tmp);

            gestionnaireDesElementsEnJeu.ajouterObject(gameData.getMapCourante());

        }
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Fenetree.java
 *
 * Created on 9 nov. 2011, 21:40:14
 */

package alpha_v2_vidéogame;

import alpha_v2_vidéogame.modèle.ConstantesDuJeu;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;

/**
 *
 * @author Eloi
 */
public class Fenetre extends javax.swing.JFrame implements ConstantesDuJeu {

    /** Creates new form Fenetree */
    public Fenetre() {
        initComponents();

            GraphicsEnvironment env = GraphicsEnvironment.
                getLocalGraphicsEnvironment();
            GraphicsDevice device = env.getDefaultScreenDevice();


            GraphicsConfiguration gc = device.getDefaultConfiguration();

//            this.setUndecorated(true);
            this.setIgnoreRepaint(true);
            this.setVisible(true);
           // device.setFullScreenWindow(this);

            int numBuffers =4;
            Rectangle bounds = this.getBounds();
            setLocation(100, 100);
            setSize(LARGEURZONEDEJEU, HAUTEURZONEDEJEU);
            this.createBufferStrategy(16);
        int width =LARGEURZONEDEJEU ;
        int height = HAUTEURZONEDEJEU;

    }

 
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                exitForm(evt);
            }
        });

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitForm(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_exitForm
        System.exit(0);
    }//GEN-LAST:event_exitForm

  

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alpha_v2_videogame.modèle;

import alpha_v2_videogame.modèle.personnage.EtatDeplacement;
import java.awt.Image;

/**
 *
 * @author Eloi
 */
 public abstract class ElementMovible extends ElementComposant{

    @Override
   abstract public Image getCurrentImage();
    private EtatDeplacement etatDeplacement;

    public  ElementMovible(String nom, float largeur, float hauteur, Image[] images, boolean solide) {
        super( nom,  largeur,  hauteur, images,  solide);
         etatDeplacement = new EtatDeplacement();
    }
      public ElementMovible(String nom, Image[] images,boolean solide) {
        super ( nom, images, solide);
         etatDeplacement = new EtatDeplacement();

    }


        /**
     * Permet au personnage de sauter selon un critère de impulsion (vitesse initial)
     * @param value
     */
    public void jump(int value) {
        etatDeplacement.setMoving(true);
        etatDeplacement.setFalling(true);
        etatDeplacement.setVitesseVerticalAvecControl(-Math.abs(value));


    }
    public void touchTheGround()
    {

        etatDeplacement.setFalling(false);


    }



    public EtatDeplacement getEtatDeplacement() {
        return etatDeplacement;
    }

    public void setEtatDeplacement(EtatDeplacement etatDeplacement) {
        this.etatDeplacement = etatDeplacement;
    }


    public boolean isHeMoving() {
        return etatDeplacement.isHeMoving();

    }

    public boolean isHeFalling() {
        if (etatDeplacement.getVitesseVerticalAvecAccélération() >= 0) {
            return true;
        }
        return false;

    }

    public void setVitesseHorizontal(int value) {
        etatDeplacement.setVitesseHorizontalAvecControl(value);
    }

    public void setVitesseVertical(int value) {
        etatDeplacement.setVitesseVerticalAvecControl(value);
    }

    public float getVitesseHorizontal() {
        return etatDeplacement.getVitesseHorizontalAvecAccélération() ;
    }

    public float getVitesseVertical() {
        return etatDeplacement.getVitesseVerticalAvecAccélération();
    }

    public void inverseVitesseHV() {
        inverseVitesseHV(Direction.PASDIRECTION);
    }

    public void inverseVitesseHV(Direction i) {
        if (i == Direction.PASDIRECTION
                || i == Direction.OUEST
                || i == Direction.EST
                ) {
            etatDeplacement.setVitesseHorizontalAvecControl(-etatDeplacement.getVitesseHorizontalAvecAccélération());
        }
        if (i == Direction.PASDIRECTION
                || i == Direction.NORD
                || i == Direction.SUD
                ) {
            etatDeplacement.setVitesseVerticalAvecControl(-etatDeplacement.getVitesseVerticalAvecAccélération());
        }
    }
    public void setXY() {
        setXY(getPositionX(),getPositionY());
    }
    public void setXY(float x, float y) {
        setPositionX(x);
        setPositionY(y);
        etatDeplacement.refreshVitesse();

    }


    public void setMoving(boolean b) {
        etatDeplacement.setMoving(b);
    }

    public void resetMoving() {
        setMoving(false);

    }

    public void setFalling(boolean b) {
        etatDeplacement.setFalling(b);
    }



}

package vgqd.demineur.vAlpha.ressources;

import java.util.ArrayList;

public class RessourceManager {

	public RessourceManager()
	{
		init();
	}
	
	/** � placer autre par plus tard */
	private void init() {
		addRessource("Mine", "Devrait exploser si l'on approche trop");
		addRessource("Allier", "Attaque les ennemies et suit les drapeaux");
		addRessource("Ennemi", "Attaque  les alliers  qui passe � leur port�e");
		addRessource("Ennemi", "Haahah");
		}

	private void addRessource(String nom, String description) {
		boolean alreadyExiste = false;
		int i = ressourcesStocked.size()-1;
		while (alreadyExiste == false && i >= 0) {
			if (ressourcesStocked.get(i).getName().equals(nom))
				alreadyExiste = true;
			i--;
		}
		if (alreadyExiste == false) {
			Ressource rtmp = new Ressource();
			rtmp.setName(nom);
			rtmp.setDescription(description);
			ressourcesStocked.add(rtmp);
		}
	}

	private ArrayList<IRessource> ressourcesStocked= new ArrayList<>();

	public IRessource[] getAll() {
		return ressourcesStocked.toArray(new IRessource[0]);
	}

}

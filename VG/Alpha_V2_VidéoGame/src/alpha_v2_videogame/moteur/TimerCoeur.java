/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alpha_v2_videogame.moteur;

import alpha_v2_vidéogame.controleur.Controleur;
import alpha_v2_vidéogame.controleur.ControleurPrincipal;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import javax.swing.Timer;

/**
 *La classe TimerCoeur, permet d'envoyer des signales en permanence demendant
 * soit le calcule graphique des éléments(dessin, repaint,rafraichissement),
 * soit le calcule physique (déplacement, collisiont ..)
 * soit la demande de réaction de l'intelligence artificielle.
 * @author Eloi & Fab
 */
public final class TimerCoeur extends Controleur implements ActionListener {

    /** représente le temps écouler depuis le début de la partie (sans les pauses) */
    private long timeSinceStart;
    /** représente le temps écouler depuis le début de la partie (sans les pauses) */
    private long timeWhenStart;

    /** Représente le temps entre deux évènements graphiques*/
    private int unitéTempsEnMSGraphique;
    /** Représente le temps entre deux évènements physique*/
    private int unitéTempsEnMSPhysique;
    /** Représente le temps entre deux évènements d'intelligence artificiel*/
    private int unitéTempsEnMSIA;


    private long lastTimeGraphique;
    private long lastTimePhysique;

    private Timer [] timers;
    private ControleurPrincipal controlPrincipal;

    public TimerCoeur(ControleurPrincipal controleurAbonnéeAuTimer,int TimeMSGraphique,int TimeMSPhysique,int TimeMSIA)
    {
        super();
        timeSinceStart=0;
        this.unitéTempsEnMSGraphique = TimeMSGraphique;
        this.unitéTempsEnMSPhysique = TimeMSPhysique;
        this.unitéTempsEnMSIA = TimeMSIA;
        controlPrincipal= controleurAbonnéeAuTimer;

        timers = new Timer[3];
        timers[0] = new Timer(unitéTempsEnMSGraphique, this );
        timers[0].setActionCommand("SignalGraphique");

        timers[1] = new Timer(unitéTempsEnMSPhysique, this );
        timers[1].setActionCommand("SignalPhysique");

        timers[2] = new Timer(unitéTempsEnMSIA, this );
        timers[2].setActionCommand("SignalIA");


        timeWhenStart=Calendar.getInstance().getTimeInMillis();
         pause();



    }

    public void play (){
        for (Timer tmp : timers)
            tmp.start();
    }
    public void pause (){
        for (Timer tmp : timers)
            tmp.stop();
    }

    private int compteurSeconde =0;
    public void actionPerformed(ActionEvent e) {


        if (e.getActionCommand().equals("SignalGraphique"))
        {

             timeSinceStart = e.getWhen() - timeWhenStart;

            lastTimeGraphique = e.getWhen();
             controlPrincipal.signalCalculGraphique(e);

            if (compteurSeconde <(timeSinceStart )/1000.0){
                compteurSeconde++;
                notifyChange("time:"+compteurSeconde+":"+getTimeSinceStartApp());
            }

        }
         else if (e.getActionCommand().equals("SignalPhysique"))
        {

            lastTimePhysique = e.getWhen();
               controlPrincipal.signalCalculPhysique(e);
        }
         else if (e.getActionCommand().equals("SignalIA"))
        {
               controlPrincipal.signalCalculIA(e);
        }

    }

    public long getTimeSinceStartApp() {
            return timeSinceStart ;
    }

    public int getUnitéDeTempsPhysique() {
        return unitéTempsEnMSPhysique;
    }

    public long getLastTimeGraphique() {
        return lastTimeGraphique;
    }

    public long getLastTimePhysique() {
        return lastTimePhysique;
    }


   //////////////////////////////////Partie static de la classe




}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nuagequivolenetombepas.modèle;


import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eloistree
 */
public class MotAffiché extends MotPesé implements Cloneable
{
    private boolean isHorizontal;
    //private java.awt.Font style;
    private int size;
    private String police;
    private java.awt.Color couleur;
    // tant que couleur perso est vrai, on ne peut modifier le couleur.
    private boolean couleurPersonnalisée;
    private boolean border;
   
   
    
    public MotAffiché(String PE_mot, int PE_occurrence)
    {
        super(PE_mot,PE_occurrence);
        isHorizontal=true;
        police="Arial";
        couleur=Color.BLACK;
        size=12;

     
    }
    
    
    public void setIsHorizontal(boolean value)
    {
        isHorizontal= value;
        
        setChanged();
        notifyObservers();
    
    }
    public boolean isHorizontal()
    {
        return isHorizontal;
    
    }

    public Color getCouleur() {
        return couleur;
    }

    public void setCouleur(Color couleur) {
        if(!couleurPersonnalisée)
        this.couleur = couleur;
        setChanged();
        notifyObservers();
    }

    public String getPolice() {
        return police;
    }

    public void setPolice(String police) {
        this.police = police;
        
        setChanged();
        notifyObservers();
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
        
        setChanged();
        notifyObservers();
    }

    public boolean isCouleurPersonnalisée() {
        return couleurPersonnalisée;
    }

    public void setCouleurPersonnalisée(boolean couleurPersonnalisée) {
        this.couleurPersonnalisée = couleurPersonnalisée;
    }

    public boolean isBorder() {
        return border;
    }

    public void setBorder(boolean border) {
        this.border = border;
        
        setChanged();
        notifyObservers();
    }
    
    
    
  
    
     
        
      
    @Override
       public int compareTo(Object o) {

        if (this==o) return 0;
          String a = this.getMot();
          String b = ((MotAffiché) o).getMot(); 
          return a.compareTo(b);
        }
    
    
    @Override
    public String toString()
    {
        String tmp ="\""+ super.toString();
        tmp+="("+size+"px,"+police+","+couleur+")\"";
        
    
    return tmp;
    }
   
  
    

    public boolean isVertical() {
        return !isHorizontal;
    }
    
    public MotAffiché getACopy()
    {
        MotAffiché newObject = new MotAffiché(getMot(), getOccurrence());
        newObject.setBorder(isBorder());
        newObject.setIsHorizontal( isHorizontal());
        newObject.setCouleurPersonnalisée(isCouleurPersonnalisée());
        newObject.setPolice(getPolice());
        newObject.setSize(getSize());
        newObject.setOccurrence(getOccurrence());
        newObject.setTauxPopularité(getTauxPopularité());
        
        Color tmpColor= getCouleur();
        newObject.setCouleur(new Color(tmpColor.getRed(),tmpColor.getGreen(),tmpColor.getBlue()));
        
                
    
    return newObject;
    }
    public void signalerSuppression()
    {
    
            setChanged();
            String autoDestruction="autoDesctrution";
            notifyObservers(autoDestruction);
            deleteObservers();
            
            
    }
    
   
    @Override
    public void finalize()
    {
        try {
         
            signalerSuppression();
            super.finalize();
        } catch (Throwable ex) {
            Logger.getLogger(MotAffiché.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }

    public void updateNow() {
        setChanged();
        notifyObservers();
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.time;

import demineur3.controleur.CONSTANTDUJEU;
import demineur3.controleur.Gestionnaire.Gestionnaire;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;


/**
 *.
 * Le role de temps est d'envoyer des signaux à l'application correspondant au temps qui passe  pour la mise à jours de l'affichage, le déplacement des personnages, les animations ...
 * @author christine
 */
public class Temps implements ActionListener {
    
    private  Timer timer;
    private int i;
    
    {
        i=0;
        timer = new Timer(1000/CONSTANTDUJEU.FRAMEPERSECONDEOFTHEGAME, this);
        timer.start();
    }

    public Temps() {
        timer.addActionListener(this);
    }
    
    
    public void ajouterUnEcouteur(   Gestionnaire gestionnaire)
    {
        timer.addActionListener(gestionnaire);
     }
     public void supprimerUnEcouteur(   Gestionnaire gestionnaire)
    {
        timer.removeActionListener(gestionnaire);
     }
    

    @Override
    public void actionPerformed(ActionEvent e) {
        i++;
        
    }

    public void pause() {
        timer.stop();
    }

    public void play() {
        timer.start();
    }
    
    
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alpha_v2_vidéogame.controleur;

import alpha_2v_videogame.modèle.carte.CollectionTuile;
import alpha_2v_videogame.modèle.carte.Map;
import alpha_v2_videogame.modèle.personnage.Personnage;
import alpha_v2_vidéogame.modèle.ConstantesDuJeu;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Eloi
 */
public class GameData  extends Controleur implements ConstantesDuJeu {

    private Map mapCourante;
    private Image background;
    private GestionnaireDeNiveau maps;
    private Personnage mainCharacter;
    private ArrayList<Personnage> personnages;
    static public CollectionTuile toutesLesTuiles;

    static {
        toutesLesTuiles = new CollectionTuile();
    }
    
      {
        personnages = new ArrayList<Personnage>();
        initialiserDeGameDataEnAttendantMieux();
    }

    public GameData(ControleurPrincipal aThis) {
        super();


    }


    private void initialiserDeGameDataEnAttendantMieux() {

         background=  new javax.swing.ImageIcon(getClass().getResource("/alpha_v2_videogame/ressources/images/Space_Rainbow_desktop_background_pictures.jpg")).getImage();

           Image [] imageDuPersonnage = new Image[1];
            imageDuPersonnage[0] =  new javax.swing.ImageIcon(getClass().getResource("/alpha_v2_videogame/ressources/images/mario1.gif")).getImage();

           Image [] animation = new Image[3];
            animation[0] =  new javax.swing.ImageIcon(getClass().getResource("/alpha_v2_videogame/ressources/images/mario1.gif")).getImage();
            animation[1] =  new javax.swing.ImageIcon(getClass().getResource("/alpha_v2_videogame/ressources/images/mario2.gif")).getImage();
            animation[2] =  new javax.swing.ImageIcon(getClass().getResource("/alpha_v2_videogame/ressources/images/mario3.gif")).getImage();

        mainCharacter = new Personnage("Mario",imageDuPersonnage[0].getWidth(null),imageDuPersonnage[0].getHeight(null),imageDuPersonnage , true);
        mainCharacter.setAnimation(animation, 6);
        mainCharacter.setPositionX(200);
        mainCharacter.setPositionY(200);
        mainCharacter.getEtatDeplacement().setVitesseMin(10);
        mainCharacter.getEtatDeplacement().setVitesseHorizontalAvecControl(0);
        mainCharacter.jump(100);

        addPersonnage(mainCharacter);
              mainCharacter = new Personnage("A",imageDuPersonnage[0].getWidth(null),imageDuPersonnage[0].getHeight(null),imageDuPersonnage , true);
        mainCharacter.setAnimation(animation, 1);
        mainCharacter.setPositionX(300);
        mainCharacter.setPositionY(130);
        mainCharacter.getEtatDeplacement().setVitesseMin(10);
        mainCharacter.getEtatDeplacement().setVitesseHorizontalAvecControl(-10);
        mainCharacter.jump(100);

        addPersonnage(mainCharacter);
              mainCharacter = new Personnage("B",imageDuPersonnage[0].getWidth(null),imageDuPersonnage[0].getHeight(null),imageDuPersonnage , true);
        mainCharacter.setAnimation(animation, 12);
        mainCharacter.setPositionX(600);
        mainCharacter.setPositionY(180);
        mainCharacter.getEtatDeplacement().setVitesseMin(10);
        mainCharacter.getEtatDeplacement().setVitesseHorizontalAvecControl(-20);
        mainCharacter.jump(100);

        addPersonnage(mainCharacter);

        mainCharacter = new Personnage("Luidgi",imageDuPersonnage[0].getWidth(null),imageDuPersonnage[0].getHeight(null),imageDuPersonnage , true);
        mainCharacter.setAnimation(animation, 2);
        mainCharacter.setPositionX(TAILLE_TUILE);
        mainCharacter.setPositionY(TAILLE_TUILE);

        addPersonnage(mainCharacter);


        int width =25   , height = 20;
        int[][] unLevel = new int[width][height];

        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++) {
                unLevel[i][j] = 0;
                if (j == height - 1 || i == width - 1 || j == 0 || i == 0) {
                    unLevel[i][j] = 1;
                }

            }

        unLevel[3][10] = 1;
        unLevel[4][6] = 1;
        unLevel[5][14] = 1;
        unLevel[1][4] = 1;
        unLevel[2][11] = 1;
        unLevel[8][2] = 1;

       setMapCourante(new Map("Map pour les nulls",unLevel,width,height, toutesLesTuiles.getCollectionDeTuile()));


    }

    public Map getMapCourante() {
        return mapCourante;
    }

    public void setMapCourante(Map value) {
        this.mapCourante = value;

        notifyChange("map:"+value.getNom());
    }

    public Personnage getMainCharacter() {
        return mainCharacter;

    }
       public void setMainCharacter(Personnage perso) {
        mainCharacter =perso;

    }

    public List<Personnage> getPersonnages() {
      return personnages;
    }

    public void addPersonnage(Personnage value) {
        personnages.add(value);
        notifyChange("personnage:"+value.getNom());

    }

    public Image getBackground() {
        return background;
    }

    public void setBackground(Image background) {
        this.background = background;
    }

}

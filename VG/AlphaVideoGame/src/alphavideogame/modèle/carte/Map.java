/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alphavideogame.modèle.carte;

import alphavideogame.modèle.ConstantesDuJeu;
import alphavideogame.modèle.ElementComposant;
import alphavideogame.modèle.personnage.Personnage;
import java.util.ArrayList;
import java.util.HashMap;


/**
 *
 * @author Eloi & Fab
 */
public class Map implements ConstantesDuJeu {

    public static float getLimitTuileCourante(float[]point, boolean limteHorizontal, boolean limteInférieur ) {
       int p_X=((int)point[0])/TAILLE_TUILE;
       int p_Y=((int)point[1])/TAILLE_TUILE;
       if (limteHorizontal && limteInférieur ) return (p_X) *TAILLE_TUILE;
       if(limteHorizontal && !limteInférieur) return (p_X+1) * TAILLE_TUILE;
        if(!limteHorizontal && limteInférieur) return (p_Y) * TAILLE_TUILE;
        if(!limteHorizontal && !limteInférieur) return (p_Y+1) * TAILLE_TUILE;
        return -1;
    }

    private int colomn ;
    private int ligne ;
    private int[][] level;
    private HashMap<Integer, Tuile> collectionDeTuille;

   
    public Map(int ligne, int colomn, HashMap<Integer, Tuile> collectionTuile) {
        this.collectionDeTuille = collectionTuile;
        this.colomn = colomn;
        this.ligne = ligne;



        for (int i = 0; i < ligne; i++) 
            for (int j = 0; j < colomn; j++) {
                level[i][j] = 0;
            }


        

    }

    public Map(int[][] level,int ligne, int colomn,HashMap<Integer, Tuile> collectionTuile) {
        this.collectionDeTuille = collectionTuile;
        this.level = level;
        this.ligne = ligne;
        this.colomn = colomn;


    }

    public int getLargeur() {
        return colomn *TAILLE_TUILE;

    }

    public int getHauteur() {
        return ligne * TAILLE_TUILE;

    }

    public int getColomn() {
        return colomn;
    }


    public int getLigne() {
        return ligne;
    }

    public int getIdTo(int ligne, int colonne)
    {
        if (  (ligne< this.ligne && ligne>=0)
                &&
              (colonne< this.colomn && colonne>=0)
                
                )
        return level[ligne][colonne];
        else return -1;


    }

     public boolean pointInTheMap(Personnage perso, float [] p)
    {

        if ((p[0]  >= 0 && p[0] + perso.getLargeur()< this.getLargeur() )
                && (p[1]  >= 0 && p[1] + perso.getHauteur()< this.getHauteur()) ) {
            return true;
        }
        return false;
    }

     private ArrayList<Tuile> tmpArray  = new ArrayList<Tuile>();
     public   ArrayList<Tuile>  getAllTuilesTouch(ElementComposant element, float [] p)
     {
         tmpArray.clear();
         int hauteurEnTuille = (int)(((int) element.getHauteur())/TAILLE_TUILE)+1;
         int largeurEnTuille = (int)(((int) element.getLargeur())/TAILLE_TUILE)+1;
         int p_X=((int)p[0])/TAILLE_TUILE;
         int p_Y=((int)p[1])/TAILLE_TUILE;

         for(int i = p_X; i<= p_X+largeurEnTuille;i++)
           for(int j = p_Y; j<= p_Y+hauteurEnTuille;j++)
           {
               int tuileID = getIdTo(i, j);
               if(tuileID>=0)
               {
                   if (tmpArray.indexOf( collectionDeTuille.get(tuileID) )<0)
                   tmpArray.add( collectionDeTuille.get(tuileID));
               }
               
           }
         return tmpArray;
     }

    

}
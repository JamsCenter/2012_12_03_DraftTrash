/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.controleur;

/**
 *
 * @author christine
 */
public class CONSTANTDUJEU {
    static public final String TITREDUJEU = "DEMINEUR 3";
    static public final int FRAMEPERSECONDEOFTHEGAME = 30;
    
    
    static public final int ECRANJEULARGEURPARDEFAUT = 800;
    static public final int ECRANJEUHAUTEURPARDEFAUT = 600;
    
    static public final String KEYUP="z";
    static public final String KEYLEFT="d";
    static public final String KEYDOWN="s";
    static public final String KEYRIGHT="q";
    
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alpha_v2_vidéogame.controleur;

import alpha_2v_videogame.modèle.carte.Map;
import alpha_v2_videogame.modèle.ElementUse;
import alpha_v2_videogame.modèle.personnage.Personnage;
import alpha_v2_vidéogame.Fenetre;
import java.awt.event.ActionEvent;


/**
 *
 * @author Eloi
 */
public class ControleurGraphique  extends Controleur  {

    private ControleurPrincipal controleurPrincipal;
    private ObjectInScreen gestionnaireDesElementsEnJeu;
    private Fenetre fenetre;

    {

    }

    public ControleurGraphique(ControleurPrincipal aThis, ObjectInScreen gestionnaireDesElementsEnJeu,Fenetre fenetre) {
    super();
    this.fenetre = fenetre;
    this.controleurPrincipal = aThis;
    this.gestionnaireDesElementsEnJeu= gestionnaireDesElementsEnJeu;


        PaintElement.setBufferWith( fenetre.getBufferStrategy());

    }

    void signalCalcul(ActionEvent e) {


        PaintElement.setBuffer();

        for (ElementUse tmp :gestionnaireDesElementsEnJeu.getDécor())
        {
            if (tmp.getObject() instanceof Map)
            {
                PaintElement.printThisInBuffer((Map)tmp.getObject() );
            }

            tmp.decrement();
        }

        for (ElementUse tmp :gestionnaireDesElementsEnJeu.getMovible())
        {
            if (tmp.getObject() instanceof Personnage)
            {
                PaintElement.printThisInBuffer((Personnage) tmp.getObject());

            }

        }

        PaintElement.print();

        gestionnaireDesElementsEnJeu.supprimerObjetRésidu();


    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alphavideogame.controleur.affichage;

import alphavideogame.controleur.ControleurPrincipal;
import alphavideogame.modèle.ObjectToRefresh;
import alphavideogame.modèle.personnage.Personnage;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import javax.swing.JPanel;
import mécanisme.SérieDeMéthodeIndépendante;

/**
 *
 * @author Eloi & Fab
 */
public class ControleurRafraichissement{

    private ArrayList<ObjectToRefresh> listToRefresh;
    private ArrayList<ObjectToRefresh> elementToSupprimer;
    private ControleurPrincipal controlerPrincipal;
    private ControleurDeCollision controlerCollision;
    private PaintObjectToJPanel printer;

    {
        printer = new PaintObjectToJPanel();
    }

    public ControleurRafraichissement(ControleurPrincipal controlerPrincipal) {
        this.controlerPrincipal = controlerPrincipal;
        listToRefresh = new ArrayList<ObjectToRefresh>();
        elementToSupprimer = new ArrayList<ObjectToRefresh>();
        controlerCollision = controlerPrincipal.getControlerCollision();



    }

    public void ajouterObject(Object o) {
        listToRefresh.add(new ObjectToRefresh(o));

    }

    public void ajouterObject(Object o, int pourNombreFrameX) {
        listToRefresh.add(new ObjectToRefresh(o, pourNombreFrameX));
    }

    public void refreshPosition(ActionEvent e) {


        for (ObjectToRefresh objToRefresh : listToRefresh) {


            if (objToRefresh.getObject() instanceof Personnage) {

              Personnage tmpPerso = (Personnage) objToRefresh.getObject();

              // il serait plus propre de récupérer le temps depuis le dernière évènement
              // stocker les dernières évènements, et comparer pour avoir le temps passé
              controlerCollision.gestionDuDéplacementDe(tmpPerso,SérieDeMéthodeIndépendante.getUnitéTemps());
            }

        }



    }

    public void refreshGraphics() {

        if (listToRefresh != null && listToRefresh.size() > 0) {
            JPanel plateformeJPanel = controlerPrincipal.getFenetre().getJPanelPlateforme();
            Graphics plateforme = plateformeJPanel.getGraphics();

            Image image = plateformeJPanel.createImage(plateformeJPanel.getWidth(), plateformeJPanel.getHeight());
            Graphics buffer = image.getGraphics();

            boolean unObjetPourLaPlatforme = false;

            for (ObjectToRefresh objToRefresh : listToRefresh) {

                if (objToRefresh.isToDelete()) {
                    elementToSupprimer.add(objToRefresh);
                } else {
                    if (objToRefresh.getObject() instanceof alphavideogame.modèle.carte.Map) {
                        unObjetPourLaPlatforme = true;
                        printer.printObjectToThisGraphics(buffer, (alphavideogame.modèle.carte.Map) objToRefresh.getObject());
                    } else if (objToRefresh.getObject() instanceof Personnage) {



                        printer.printObjectToThisGraphics(buffer, (Personnage) objToRefresh.getObject());
                    }


                    objToRefresh.decrement();
                }
            }
            if (unObjetPourLaPlatforme) {
                plateforme.drawImage(image, 0, 0, plateformeJPanel);
            }
        }

    }

    public void supprimerObjetRésidu() {

        for (ObjectToRefresh o : elementToSupprimer) {

            listToRefresh.remove(o);
            System.out.println("supprimer: " + o);
        }
        elementToSupprimer.clear();

    }


}

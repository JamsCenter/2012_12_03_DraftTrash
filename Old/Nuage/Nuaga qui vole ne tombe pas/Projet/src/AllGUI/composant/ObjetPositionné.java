
package AllGUI.composant;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.border.Border;
import nuagequivolenetombepas.modèle.MotAffiché;
 
public class ObjetPositionné extends JLabel implements Observer {
    public final static int ROTATE_RIGHT = 1;
    public final static int DONT_ROTATE = 0;
    public final static int ROTATE_LEFT = -1;
    private int rotation = DONT_ROTATE;
    private boolean painting = false;
 
  
 
    public ObjetPositionné() {
      super();
        
      
       
    }
 
   
    public int getRotation() {
        return rotation;
    }
 
    public void setRotation(int rotation) {
        this.rotation = rotation;
    }
 
    public boolean isRotated() {
        return rotation != DONT_ROTATE;
    }
 
    @Override
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
 
        if (isRotated()) {
            g2d.rotate(Math.toRadians(90 * rotation));
        }
 
        if (rotation == ROTATE_RIGHT) {
            g2d.translate(0, -this.getWidth());
        } else if (rotation == ROTATE_LEFT) {
            g2d.translate(-this.getHeight(), 0);
        }
 
        painting = true;
        super.paint(g2d);
        painting = false;
 
        if (isRotated()) {
            g2d.rotate(-Math.toRadians(90 * rotation));
        }
 
        if (rotation == ROTATE_RIGHT) {
            g2d.translate(-this.getWidth(), 0);
        } else if (rotation == ROTATE_LEFT) {
            g2d.translate(0, -this.getHeight());
        }
    }
 
   
 
    @Override
    public int getWidth() {
        if ((painting) && (isRotated())) {
            return super.getHeight();
        }
        return super.getWidth();
    }
 
    @Override
    public int getHeight() {
        if ((painting) && (isRotated())) {
            return super.getWidth();
        }
        return super.getHeight();
    }
 
    @Override
    public Dimension getPreferredSize() {
        Dimension d = super.getPreferredSize();
        if (isRotated()) {
            int width = d.width;
            d.width = d.height;
            d.height = width;
        }
        return d;
    }
 
    @Override
    public Dimension getMinimumSize() {
        Dimension d = super.getMinimumSize();
        if (isRotated()) {
            int width = d.width;
            d.width = d.height;
            d.height = width;
        }
        return d;
    }
 
    @Override
    public Dimension getMaximumSize() {
        Dimension d = super.getMaximumSize();
        if (isRotated()) {
            int width = d.width;
            d.width = d.height;
            d.height = width;
        }
        return d;
    }

    @Override
    
    
    public void update(Observable o, Object o1) {
        
        if (o1 instanceof String)
        {
            if (o1.equals("autoDesctrution"))
            {
            System.out.println("Suppression : "+ (String )o1);
            Component tmpParent = this.getParent();
            this.getParent().remove(this);
            tmpParent.repaint();
            }
        }
        else 
        {

            System.out.println("Mise à jours :"+o);

           if( o instanceof MotAffiché)
           {
            MotAffiché tmp = (MotAffiché) o;

           setText(tmp.getMot());
           Font tmpp=new Font(tmp.getPolice(),1,tmp.getSize());
           this.setFont(tmpp );
           

           this.setForeground(tmp.getCouleur());
           
           if(this.getGraphics()!=null) {
           Graphics2D g2 = (Graphics2D) this.getGraphics();

            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setFont(getFont());

            FontRenderContext frc = g2.getFontRenderContext();
            String s = getText();
            Rectangle2D bounds = g2.getFont().getStringBounds(s, frc);
            this.setSize(
                    ((int) bounds.getWidth())+4,
                     ((int) bounds.getHeight())+4
                    );
           }
           if (tmp.isBorder()){
           this.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 0, 0), 1, true));


        
           
           }else 
           { this.setBorder(null);
              
           }
           
           


           if(tmp.isVertical())
           {
               this.setRotation(WIDTH);
               this.setSize(getHeight(),getWidth());


           }
           else {  
                this.setRotation(0);
               this.setSize(getWidth(),getHeight());
           }

           this.validate();
         



           }
       }
    }

}
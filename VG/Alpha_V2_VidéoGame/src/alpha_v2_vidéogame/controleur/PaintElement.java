/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alpha_v2_vidéogame.controleur;

import alpha_2v_videogame.modèle.carte.Map;
import alpha_v2_videogame.modèle.personnage.EtatDeplacement;
import alpha_v2_videogame.modèle.personnage.Personnage;
import alpha_v2_vidéogame.modèle.ConstantesDuJeu;
import alpha_v2_vidéogame.mécanisme.FloatPoint;
import alpha_v2_vidéogame.mécanisme.SérieDeMéthodeStatic;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferStrategy;
import javax.swing.JPanel;

/**
 *
 * @author Eloi
 */
public class PaintElement implements ConstantesDuJeu{
    static private JPanel jpanTmp;
    static private Image imageTmp;
    static private Graphics2D bufferTmp;
    static private Graphics source;
    static private BufferStrategy myStrategy ;

    /*

     BufferStrategy myStrategy;

while (!done) {
    Graphics g;
    try {
        g = myStrategy.getDrawGraphics();
        render(g);
    } finally {
        g.dispose();
    }
    myStrategy.show();
}
     */

   static public void paintThisImageIn(JPanel jPanel,Image img)
    {
        //source = jPanel.getGraphics();
        //source.drawImage(imageTmp, 0, 0, jPanel);
         //
    }
     static void setBufferWith(BufferStrategy bufferStrategy) {
        PaintElement.myStrategy = bufferStrategy;
    }
     static public void setBuffer() {

            myStrategy.show();
            bufferTmp  =(Graphics2D) myStrategy.getDrawGraphics();


            bufferTmp.fillRect(0, 0, LARGEURZONEDEJEU , HAUTEURZONEDEJEU);

    }



    /** dessine la carte dans le buffer*/
    static public void printThisInBuffer(Map map) {
         for(int i =0; i<map.getLigne();i++)
            {
                for(int j =0; j<map.getColomn();j++)
                {
                    Image tmp = GameData.toutesLesTuiles.getTuilleFor(map.getIdTo(i, j)).getImage(0);
                    if (tmp!=null)
                    {

                        bufferTmp.drawImage(tmp, i * TAILLE_TUILE, j * TAILLE_TUILE, null);
                    }
                }
            }

             for(int i =0; i<map.getColomn();i++)
            {
                bufferTmp.drawLine(0,i*(TAILLE_TUILE)-1,map.getHauteur()-1, i*(TAILLE_TUILE)-1);

            }
             for(int i =0; i<map.getLigne();i++)
             {
                   bufferTmp.drawLine(i*(TAILLE_TUILE)-1, 0, i*(TAILLE_TUILE)-1, map.getLargeur()-1);


              }

      
    }

    /** dessine la carte dans le personnage*/
    static public void printThisInBuffer(Personnage p) {
                   FloatPoint tmpP = EtatDeplacement.getPositionOfThis(p.getPosition(),
                        p.getEtatDeplacement(),
                        (int) SérieDeMéthodeStatic.getTimeBetweenGraphAndPhysicEvent());

                    Image tmp = p.getCurrentImage();
                    if (tmp!=null)
                   bufferTmp.drawImage(tmp,(int) tmpP.x, (int)tmpP.y,null );
                   bufferTmp.drawRect((int)tmpP.x,(int) tmpP.y,(int)p.getLargeur(), (int)p.getHauteur());
                   bufferTmp.drawRect((int)tmpP.x,(int) tmpP.y,TAILLE_TUILE, 2*TAILLE_TUILE);


    }


    /** dessine finalement dans le jpanel de départ, le buffer résultant des prints*/
    static public void print() {


        bufferTmp.dispose();
        myStrategy.show();
        
        //source.drawImage(imageTmp, 0, 0, jpanTmp);
        
    }

}

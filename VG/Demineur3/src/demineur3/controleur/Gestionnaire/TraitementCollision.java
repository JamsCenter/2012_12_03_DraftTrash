/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.controleur.Gestionnaire;

import demineur3.controleur.Controleur;
import demineur3.donnees.ModèleElementsCarte;
import demineur3.donnees.ModèleInterface;
import demineur3.donnees.ModèleJoueurs;
import demineur3.donnees.ModèlePartie;
import demineur3.modeleDeDonnee.Element;
import demineur3.modeleDeDonnee.Mine;
import demineur3.modeleDeDonnee.Personnage;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 *
 * @author java06
 */
public class TraitementCollision {
    
    public void collision(ModèleInterface materiel, ModèleElementsCarte carte, ModèleJoueurs joueurs, ModèlePartie niveaux, int frameSinceStart) {
        
        collisionElemetMines(joueurs.getJoueur(), materiel, carte, joueurs);
        
    }
    
    public void collisionElemetMines(Element elem, ModèleInterface materiel, ModèleElementsCarte carte, ModèleJoueurs joueurs) {
        
        for (Mine mine  : carte.getAllMinesOn(elem.getPosition())) {
            mine.setActiver(true);
        }
        
    }
   
    
}

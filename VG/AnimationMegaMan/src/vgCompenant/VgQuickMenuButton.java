package vgCompenant;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JFrame;

 class VgQuickMenuButton extends JButton implements ActionListener, MouseListener {

	private Image green = new javax.swing.ImageIcon(getClass().getResource(
			"/vgCompenant/greenButton.png")).getImage();
	private Image orange = new javax.swing.ImageIcon(getClass().getResource(
			"/vgCompenant/orangeButton.png")).getImage();
	private Image yellow = new javax.swing.ImageIcon(getClass().getResource(
			"/vgCompenant/yellowButton.png")).getImage();
	private Image red = new javax.swing.ImageIcon(getClass().getResource(
			"/vgCompenant/redButton.png")).getImage();

	public Couleur c = Couleur.green;

	public enum Couleur {
		green, orange, yellow, red;

	}
	

	public VgQuickMenuButton() {
		super();
		this.addActionListener(this);
		this.addMouseListener(this);
	}

	

	private static final long serialVersionUID = 737763470422054707L;

	@Override
	public void paint(Graphics g) {
		if (c.equals(Couleur.green))
			g.drawImage(green, 0, 0, this.getWidth(), this.getHeight(), null);
		else if (c.equals(Couleur.yellow))
			g.drawImage(yellow, 0, 0, this.getWidth(), this.getHeight(), null);
		else if (c.equals(Couleur.red))
			g.drawImage(red, 0, 0, this.getWidth(), this.getHeight(), null);

		else
			g.drawImage(orange, 0, 0, this.getWidth(), this.getHeight(), null);
		
		int hauteurPolice = this.getSize().height/4;
		g.setFont(new Font("Arial", 0, hauteurPolice));
		
		g.drawString(this.getText(), (this.getSize().width/2)-(hauteurPolice/3*getText().length()), (this.getSize().height/2)+5-hauteurPolice/2);

	}
	
	public static void main(String[] args) {
		JFrame f  = new JFrame();
		
		f.setVisible(true);
		f.setLayout(new GridLayout());
		VgQuickMenuButton b= new VgQuickMenuButton();
		b.setText("bou");
		b.c = Couleur.orange;
		
		b.addActionListener(b);
		f.add(b);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println(""+this.getText());
		this.c = Couleur.orange;
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	private Couleur tmp;
	@Override
	public void mouseEntered(MouseEvent e) {
		tmp= c;
		this.c = Couleur.orange;
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
	c=tmp;	
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}

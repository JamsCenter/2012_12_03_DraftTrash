package vgqd.games.tuxdominationeditor.swing.gui;

import vgqd.games.tuxdomination.GestionnaireEyeesAndEars;
import vgqd.games.tuxdomination.IO.out.ExportToXML;
import vgqd.games.tuxdomination.observable.ArmyObserved;
import vgqd.games.tuxdomination.observable.UnityObserved;
//import vgqd.games.tuxdominationeditor.swing.gui.unity.UnityFullPanel;

public class JTuxEditor extends JTuxEditorInit {
	protected GestionnaireEyeesAndEars application;

	public JTuxEditor(GestionnaireEyeesAndEars geae) {
		super();
		application = geae;
		super.setTitle(title);
	}
	private String title="Tux Domination";
	public void setTitle(String value)
	{
		super.setTitle(title+": "+value);
		
	}
	
	/*
	public void update(UnityObserved uo )
	{
		panelunitee.setVie(uo.getVie());
		panelunitee.setAttack(uo.getAttack());
		panelunitee.setDefence(uo.getDefence());
		panelunitee.setPortee(uo.getPortee());
		panelunitee.setPointAction(uo.getActionPoint());
		panelunitee.validate();
		panelunitee.repaint();
		
	}*/
	
	public void update(ArmyObserved uo )
	{
		
		jTextArmy.setText(uo.getName());
		panelxml.setText(ExportToXML.makeTheText(uo.getArmy()));
		panelxml.validate();
		panelxml.repaint();
		
		
	}
	

    private void jMenuAbout() {
        //G.go on facebook>tuxdomination
    }

    private void jMenuExport() {
        
        //G.export(path);
    	
        
    }

    private void jMenuQuite() {
       System.exit(1);
    }

    private void jMenuArmy() {
       //G.selectArmies();
    }

    private void jMenuImport() {
       //g.import
    }
    private void jTextArmyKeyReleased() {
      //G.namearmychanged
    }
  

}

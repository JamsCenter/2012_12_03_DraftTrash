package vgAnimation;

import java.awt.Image;
import java.util.HashMap;

public class Animation {
	private static HashMap<String,Animation> animations = new HashMap<>();

	private Image [] images= null;
	private String nom;
	private int frame;
	
	/**  en ms*/
	private int duree;
	

	

	public Animation(String nom,Image [] imgs, int duree,int frame) {
		super();
		if (nom==null || duree<1) throw new  IllegalArgumentException("CREATION CANCELLED: Nom doit exister et dur�e doit �tre sup�rieur � 1");
		if (animations.get(nom)!=null) throw new  IllegalArgumentException("CREATION CANCELLED: Une animation poss�de d�j� ce nom");
		if (imgs==null || imgs.length<1)throw new  IllegalArgumentException("CREATION CANCELLED: Une animation doit poss�der au moins une image");
		
		this.nom = nom;
		this.duree = duree;
		this.images = imgs;
		setFrame(frame);
		animations.put(nom, this);
		
	}

	public static HashMap<String, Animation> getAnimations() {
		return animations;
	}

	public static void setAnimations(HashMap<String, Animation> animations) {
		Animation.animations = animations;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getDuree() {
		return duree;
	}
	
	public int getFrame() {
		return frame;
	}
	public void setFrame(int frame)
	{
		if (frame>images.length)
		{
			this.frame= frame;
			
		}
		else this.frame=images.length;
		
		
	}
	public double getFramePerSeconde() {
		return ((double)getFrame())/((double)duree/1000);
	}
	public double getMilliSecondePerFrame() {
		return duree/frame;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}

	@Override
	public String toString() {
		return "Animation [images=" + images.length + ", nom=" + nom
				+ ", duree=" + duree + ""		+ ", frame=" + getFrame() 		+ ", fps=" + getFramePerSeconde() + "]";
	}

	public Image[] getImages() {
		// TODO Auto-generated method stub
		return images;
	}
	
	public Image getImageForThisMS(int ms)
	{
		int i=0;
		float tmp=0;
		if (ms>0)
		{
			// in 320
			//100 ms pour une animation de 11 frame avec 4 images
			// 320%100 = 20.0
			tmp=ms%duree;
			//20/100 =1/5
			tmp=tmp/(float)duree;
			//4.0*1/5 = 
			tmp= ((float)images.length)*tmp;
		
			i= Math.round(tmp);
		}
		
		
		return images[i];
	}

	public static Animation getAnimation(String string) {

		
		return  animations.get(string);
	}
	
	
	
	
	
	
	 
	
}

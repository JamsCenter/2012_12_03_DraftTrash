package vgCompenant;

class Cellule {


	@Override
	public String toString() {
		return "[" + name + "]";
	}
	private String name="";
	private String commande="";
	private boolean selected;
	
	

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean isSelected() {
		return selected;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCommande() {
		return commande;
	}
	public void setCommande(String commande) {
		this.commande = commande;
	}
	public void reset() {
		name="";
		commande="";
		selected=false;
		
	}
	
	
	
	
}

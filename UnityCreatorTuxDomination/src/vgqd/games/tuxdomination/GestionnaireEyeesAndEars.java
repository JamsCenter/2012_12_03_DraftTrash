package vgqd.games.tuxdomination;

import java.util.Observable;
import java.util.Observer;

import vgqd.games.tuxdomination.observable.ArmyObserved;
import vgqd.games.tuxdomination.observable.UnityObserved;

public class GestionnaireEyeesAndEars implements Observer {

	private GestionnaireApp manager = null;

	public GestionnaireEyeesAndEars(GestionnaireApp manager) {
		super();
		if (manager == null)
			throw new NullPointerException();
		this.manager = manager;

	}

	@Override
	public void update(Observable o, Object arg) {

		if (o instanceof ArmyObserved) {
			ArmyObserved ao = (ArmyObserved)o;
			manager.setNameArmyCurrent(ao.getName());
			

		} else if (o instanceof UnityObserved) {
			
			
			UnityObserved uo = (UnityObserved) o;
			manager.setNameCurrent(uo.getName());
			manager.setVieCurrent(uo.getVie());
			manager.setAttackCurrent(uo.getAttack());
			manager.setDefenceCurrent(uo.getDefence());
			manager.setActionPointCurrent(uo.getActionPoint());
			manager.setPorteeCurrent(uo.getPortee());
			manager.updateDescripteurs();
			
			// dire au manager que les donn�es x
			// on �t� modifier qui pr�viendra son fils

		}

	}

}

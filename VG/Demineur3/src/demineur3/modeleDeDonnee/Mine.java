/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package demineur3.modeleDeDonnee;

import demineur3.outils.BoitAOutil;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;

/**
 *
 * @author christine
 */
public class Mine extends Explosif{
    
    protected int diametrePorteeOnde;
    protected int diametrePorteeActivation;
    
    private static int nombreDeMine=0;
    
    //private  final Image img = new javax.swing.ImageIcon(getClass().getResource("/demineur3/affichage/ressources/drapeau.jpg")).getImage() ;
    {
    nombreDeMine++;
    nom = "Mine"+nombreDeMine;
    imageParDefaul = new javax.swing.ImageIcon(getClass().getResource("/demineur3/affichage/ressources/mine.png")).getImage() ;
    dimension = new Dimension(imageParDefaul.getWidth(null),imageParDefaul.getHeight(null));
    }
    public Mine(String nom, Point position,Dimension dimension, Image image, int timer,int porteOnde,int porteeActivation ) {
        super( nom,  position, dimension, image,timer);
        if(porteOnde<1) this.diametrePorteeOnde=1;
        else this.diametrePorteeOnde = porteOnde;
        if(porteeActivation<1) this.diametrePorteeActivation=1;
        else this.diametrePorteeActivation = porteeActivation;
       
    }

    public Mine(Point position) {
        super( position);
        
       this.diametrePorteeActivation=BoitAOutil.nombreAléatoireEntreA_B(40  , 80);
       this.diametrePorteeOnde=BoitAOutil.nombreAléatoireEntreA_B(this.diametrePorteeActivation    , 150);
       
    }

    public int getDiametrePorteeActivation() {
        return diametrePorteeActivation;
    }

    public void setDiametrePorteeActivation(int porteeActivation) {
        this.diametrePorteeActivation = porteeActivation;
    }

    public int getDiametrePorteeOnde() {
        return diametrePorteeOnde;
    }

    public void setDiametrePorteeOnde(int porteeOnde) {
        this.diametrePorteeOnde = porteeOnde;
    }
    public int getPorteeActivation() {
        return diametrePorteeActivation/2;
    }

   
    public int getPorteeOnde() {
        return diametrePorteeOnde/2;
    }


    
    
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alphavideogame.modèle;

import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Eloi & Fab
 */
public class ObjectToRefresh implements Observer{

    /** Object à rafraichir*/
    private Object object;
    /** Nombre de fois  allouer à l'objet pour être rafraichit */
    private int nomberFrameRefresh;
    /** True = à supprier, false = ne pas supprimer */
    private boolean isReadyToBeRemove;
    /**  doit être rafraichit indéfiniment*/
    private boolean inifityRefresh;

    private boolean doesItHaveToBeRefresh;

    {
        doesItHaveToBeRefresh = true;
        nomberFrameRefresh = 0;
        isReadyToBeRemove = false;
        inifityRefresh = true;
    }

    /**
    Object qui a besoin d'être rafraichit en permanence

     */
    public ObjectToRefresh(Object m) {
        object = m;
    }

    /**
    Object qui a besoin d'être rafraichit un nombre de fois maximum

     */
    public ObjectToRefresh(Object m, int i) {
        object = m;
        inifityRefresh = false;
        nomberFrameRefresh = i;
    }

    /**

     * Décrément une fois à chaque rafraichissement. Si le nombre est égale à 0 ou inférieur, il pas en état de pouvoir être supprimer.
     */
    public void decrement() {
        if (!inifityRefresh) {
            nomberFrameRefresh--;
            if (nomberFrameRefresh <= 0) {
                setToDelete(true);
            }
        }
    }

    /**
     * L'objet peut il être supprimer ?
     */
    public boolean isToDelete() {
        return isReadyToBeRemove;
    }

    /**
    Définit si l'objet doit être supprimer ou non
     */
    public final void setToDelete(boolean isReadyToBeRemove) {
        this.isReadyToBeRemove = isReadyToBeRemove;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object o) {
        this.object = o;
    }

    /** supprimer un objet de la liste = setToDelete par exemple pour une mort d'un personnage
     mais doesItHaveToBeRefesh est la juste pour si le personnage doit resté dans la liste,
     * mais ne dois pas être rafraichit pour l'instant. exemple, mario ne bouge pas
     */
    public boolean doesItHaveToBeRefresh() {
        return doesItHaveToBeRefresh;
    }

    public void setDoesItHaveToBeRefresh(boolean doesItHaveToBeRefresh) {
        this.doesItHaveToBeRefresh = doesItHaveToBeRefresh;
    }

    public void update(Observable o, Object arg) {
        throw new UnsupportedOperationException("Not supported yet.");
    }



}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alpha_v2_videogame.modèle;

import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Eloi & Fab
 */
public class ElementUse implements Observer,java.lang.Comparable{

    /** Object à rafraichir*/
    private Object object;
    /** Nombre de fois  allouer à l'objet pour être rafraichit */
    private int nomberFrameRefresh;
    /** True = à supprier, false = ne pas supprimer */
    private boolean isReadyToBeRemove;
    /**  doit être rafraichit indéfiniment*/
    private boolean inifityRefresh;

    /** permet de définir si l'élément est plus important qu'un autre.
     ->cela permettra par exemple de dessiner mario en avant plan
     si l'on trie la collection
     */
    private int profondeur;

    private boolean doesItHaveToBeRefresh;

    {
        doesItHaveToBeRefresh = true;
        nomberFrameRefresh = 0;
        isReadyToBeRemove = false;
        inifityRefresh = true;
    }

    /**
    Object qui a besoin d'être rafraichit en permanence

     */
    public ElementUse(Object m) {
        object = m;
    }

    /**
    Object qui a besoin d'être rafraichit un nombre de fois maximum

     */
    public ElementUse(Object m, int i) {
        object = m;
        inifityRefresh = false;
        nomberFrameRefresh = i;
    }

    /**

     * Décrément une fois à chaque rafraichissement. Si le nombre est égale à 0 ou inférieur, il pas en état de pouvoir être supprimer.
     */
    public void decrement() {
        if (!inifityRefresh) {
            nomberFrameRefresh--;
            if (nomberFrameRefresh <= 0) {
                setToDelete(true);
            }
        }
    }

    /**
     * L'objet peut il être supprimer ?
     */
    public boolean isToDelete() {
        return isReadyToBeRemove;
    }

    /**
    Définit si l'objet doit être supprimer ou non
     */
    public final void setToDelete(boolean isReadyToBeRemove) {
        this.isReadyToBeRemove = isReadyToBeRemove;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object o) {
        this.object = o;
    }

    /** supprimer un objet de la liste = setToDelete par exemple pour une mort d'un personnage
     mais doesItHaveToBeRefesh est la juste pour si le personnage doit resté dans la liste,
     * mais ne dois pas être rafraichit pour l'instant. exemple, mario ne bouge pas
     */
    public boolean doesItHaveToBeRefresh() {
        return doesItHaveToBeRefresh;
    }

    /** l'object n'est pas supprimer mais il n'est pas redessinné pour l'instant*/
    public void setDoesItHaveToBeRefresh(boolean doesItHaveToBeRefresh) {
        this.doesItHaveToBeRefresh = doesItHaveToBeRefresh;
    }

    /**
     Le fait d'observer le modèle qu'il contient permet de faire qu'à partir du modèle, on peut lui demander
     * de ce supprimer, par exemple car donnée plus utiliser. Mais aussi de le supprimer dans un nombre de frame donnée.
     * par exemple si il y a une animation pour la mort du personnage.
     * @arg peut être: delete, deletein:20
     */
    public void update(Observable o, Object arg) {

        if(arg instanceof String)
        {
        String tmp = (String) arg;

        if(tmp.equals("delete")){ setToDelete(true);}
        else if(tmp.indexOf("deletein:")>=0)
            {
             String [] array = tmp.split(":");
             if (array.length==2){
                inifityRefresh = false;
                nomberFrameRefresh = Integer.parseInt(array[1]);
                }
            }


        }

    }

    public int getProfondeur() {
        return profondeur;
    }

    public void setProfondeur(int profondeur) {
        this.profondeur = profondeur;
    }



        public int compareTo(Object o) {
            //b.compareTo(a);
         if (this==o) return 0;
          long a = ((ElementUse) o).getProfondeur();
          long b = this.getProfondeur();
          if (a > b)  return -1;
          else if(a == b) return 0;
          else return 1;

        }




}

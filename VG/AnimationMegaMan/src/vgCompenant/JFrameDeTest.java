package vgCompenant;

import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JFrame;

public class JFrameDeTest extends JFrame implements KeyListener, MouseListener, MouseMotionListener,
		MouseWheelListener {

	private VgQuickMenuImpl menu;

	public static void main(String[] args) {

		new JFrameDeTest();
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {

		System.out.println(""+e.getWheelRotation());
		int i = e.getWheelRotation();
		while (i != 0) {

			if (i > 0) {

				menu.nextCellule();
				i--;
			}

			else if (i < 0) {
				menu.previousCellule();
				i++;
			}
		}
		menu.repaint();

	}

	public JFrameDeTest() throws HeadlessException {
		super();

		this.setLocale(null);
		menu = new VgQuickMenuImpl();

		menu.setBackground(Color.white);
		for (int i = 0; i < 10; i++) {

			menu.add("" + i, "SIGNAL:" + i);
		}
		menu.setNamePage(0, "Signal");
		menu.setNamePage(1, "Action");
		menu.setNamePage(2, "Menu");

		menu.add("help", "SIGNAL:HELP");
		menu.add("go", "SIGNAL:GO");
		menu.add("YES", "SIGNAL:YES");
		menu.add("EXIT", "MENU:QUITE", 2, 5);

		System.out.println("  " + menu);

		setSize(500,500);
		setVisible(true);

		
		add(menu);
		this.setFocusable(true);
		menu.addMouseWheelListener(this);
		menu.addMouseMotionListener(this);
		menu.addKeyListener(this);
		menu.addMouseListener(this);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		
		menu.setLocation(e.getX()-(menu.getWidth()/2), e.getY()-(menu.getHeight()/2));
		
	}

}

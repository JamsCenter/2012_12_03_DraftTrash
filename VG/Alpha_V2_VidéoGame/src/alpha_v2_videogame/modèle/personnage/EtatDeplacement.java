/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alpha_v2_videogame.modèle.personnage;

import alpha_v2_vidéogame.mécanisme.FloatPoint;
import alpha_v2_vidéogame.mécanisme.SérieDeMéthodeStatic;

/**
 *
 * @author Fabrice
 */
public class EtatDeplacement {

    /** signale que le personnage vole  */
    private boolean flyMode;
    /** est en déplacement */
    private boolean moving;
    /** entrain de tomber*/
    private boolean falling;
    private int acceleration;// par seconde
    private int decceleration; // par seconde
    /** vitesse initial de déplacement */
    private float vitesseVertical;
    /** vitesse due au temps et à l'accélération*/
    private float vitesseVerticlaAccélération;
    private float gravité;
    private float vitesseMaxGravité;
    /** vitesse initial de déplacement */
    private float vitesseHorizontal;
    /** vitesse due au temps et à la gravité*/
    private float vitesseHorizontalAccélération;
    private float vitesseMaxHorizontal;
    private float vitesseMinHorizontal;
    /** permet de savoir depuis combien de temps il sait déplacé*/
    private long tempsCourse;
    /** permet de savoir depuis combien de temps il tombe*/
    private long tempsChute;

    {
        flyMode = false;
        acceleration = 200;
        decceleration = 300;
        vitesseHorizontal = 0;
        vitesseVertical = 0;
        vitesseMaxHorizontal = 200;
        vitesseMaxGravité = 500;
        gravité = 500;
        vitesseMinHorizontal = 0;


    }

    /**
     * permet d'ajouter de la vitesse horizontal
     * @param vitesse
     */
    public void addVitesseToX(int vitesse) {
        float vitessetmp = getVitesseHorizontalAvecAccélération() + (float) vitesse;
        setVitesseHorizontalAvecControl(vitessetmp);
    }

    /**
     * permet d'ajouter de la vitesse vertical
     * @param vitesse
     */
    public void addVitesseToY(int vitesse) {
        float vitessetmp = getVitesseVerticalAvecAccélération() + (float) vitesse;
        setVitesseVerticalAvecControl(vitessetmp);

    }

    /**
     *
     * @return depuis combien de temps l'objet se déplace
     */
    public long getTempsInitialDeplacement() {

        return tempsCourse;

    }

    /**
     * permet de définit si le personnage bouge.
     * @param value
     */
    public void setMoving(boolean value) {
       
        moving = value;
        if (value&& tempsCourse<0) {
            this.tempsCourse = SérieDeMéthodeStatic.getTimeNow();
        }
        else if(value) {}
        else {
            resetMoving();
        }
       
    }

    /**
     * le personnag est il en mode déplacement
     * @return
     */
    public boolean isHeMoving() {
        return moving;
    }

    private void resetMoving() {
        vitesseHorizontal = 0;
        vitesseHorizontalAccélération=0;
        tempsCourse=-1;

    }

    /**
     * permet de dire que le personnage tombe => activation de la gravitée 
     * @param value
     */
    public void setFalling(boolean value) {
    
        falling = value;
        if (value && tempsChute<0) {
            this.tempsChute = SérieDeMéthodeStatic.getTimeNow();
        }
        else if(value) {}
        else {
            resetFalling();
        }
    }
   

    /** le personnage tombe t'il ?
     *
     * @return
     */
    public boolean isHeFalling() {
        return falling;
    }

    private void resetFalling() {
        vitesseVertical = 0;
        vitesseVerticlaAccélération=0;
        tempsChute=-1;
    }

    /**
     * le personnage accélaire si il possède un temps de course
     * @return
     */
    public boolean isAccelering() {
        if (tempsCourse <= 0) {
            return false;
        } else {
            return true;
        }

    }

    /**
     * ajouter de la vitesse vertical vers le top
     * @param vitesse
     */
    public void jump(float vitesse) {
        setVitesseVerticalAvecControl(- Math.abs(vitesse));
    }

    /**
     * retourne la vitesse initiale ajouté à la vitesse d'accélération due au temps de course
     * @return
     */
    public float getVitesseHorizontalAvecAccélération() {

        return vitesseHorizontal + vitesseHorizontalAccélération;
    }

    /** affect à la vitesse initial la valeur vitesse qui ne pourra dépassé la vitesse maximum définit
     *
     * @param vitesse
     */
    public void setVitesseHorizontalAvecControl(float vitesse) {
        setMoving(true);

        if (Math.abs(vitesse) > vitesseMaxHorizontal) {
            if (vitesse < 0) {
                vitesse = -vitesseMaxHorizontal;
            } else {
                vitesse = vitesseMaxHorizontal;
            }
        }
        else if(Math.abs(vitesse) <vitesseMinHorizontal)
        { if (vitesse < 0) {
                vitesse = -vitesseMinHorizontal;
            } else {
                vitesse = vitesseMinHorizontal;
            }
        }
        this.vitesseHorizontal = vitesse;
    }

    /**
     * retourne la vitesse initiale ajouté à la vitesse d'accélération due au temps de course
     * @return
     */
    public float getVitesseVerticalAvecAccélération() {

        return vitesseVertical + vitesseVerticlaAccélération;
    }

    /** affect à la vitesse initial la valeur vitesse qui ne pourra dépassé la vitesse maximum définit
     *
     * @param vitesse
     */
    public void setVitesseVerticalAvecControl(float vitesse) {
       
setFalling(falling);
        if (Math.abs(vitesse) > vitesseMaxGravité) {
            if (vitesse < 0) {
                vitesse = -vitesseMaxGravité;
            } else {
                vitesse = vitesseMaxHorizontal;
            }
        }
        this.vitesseVertical = vitesse;
    }

    /** permet de mettre à jour la vitesse de l'objet à partir de la durée de course ou de chute de celui ci
     * Ici, vérifie les états de déplacement et de chute avant de demandé le rafraichissement en fonction des durées calculées
     */
    public void refreshVitesse() {
        float temps;
        temps = durée(tempsCourse, SérieDeMéthodeStatic.getTimeNow());
            
                refreshVitesseWithDuréeDéplacement(temps);
            
        
            temps = durée(tempsChute, SérieDeMéthodeStatic.getTimeNow());
          
                refreshVitesseWithDuréeChute(temps);
            
        
    }

    /**
    @param  timeDebutAccélération == temps à moment du début de ça course
    @param  timeWhenAction == temps au moment du refresh
     *@return retourne la durée de l'action en seconde
     */
    public static float durée(long timeDebutAccélération, long timeWhenAction) {

        if (timeDebutAccélération >= 0 && timeWhenAction >= 0) {

            return ((float) (timeWhenAction - timeDebutAccélération)) / 1000;

        }
        return -1;
    }
      /**
    @param  timeDebutAccélération == temps à moment du début de ça course
    @param  timeWhenAction == temps au moment du refresh
     *@return retourne la durée de l'action en seconde
     */
    public static long duréeEnMS(long timeDebutAccélération, long timeWhenAction) {

        if (timeDebutAccélération >= 0 && timeWhenAction >= 0) {

            return timeWhenAction - timeDebutAccélération;

        }
        return -1;
    }

     /** permet de mettre à jour la vitesse de l'objet à partir de la durée de course ou de chute de celui ci
     * reçoit une durée et traite le rafraichissement selon l'algorithme voulu
     */
    public void refreshVitesseWithDuréeDéplacement(float tempsCourse) {
     
            //permière partie sur la vitesse horizontal
            if (acceleration != 0 && moving && tempsCourse > 0) {

                int accélération=0;
                if (vitesseHorizontal < 0.0) {
                    accélération = -acceleration;
                } else if (vitesseHorizontal > 0.0){
                    accélération = acceleration;
                }
                else  if (vitesseHorizontal == 0.0) accélération=0;


                float vitesseTmp = accélération * tempsCourse;


                if (vitesseTmp > vitesseMaxHorizontal) {
                    vitesseHorizontalAccélération = vitesseMaxHorizontal;
                } else if (vitesseTmp < -vitesseMaxHorizontal) {
                    vitesseHorizontalAccélération = -vitesseMaxHorizontal;
                } else {
                    vitesseHorizontalAccélération = vitesseTmp;
                }
            }
            // si il ne bouge plus, gestion de la décélération
             else if (!moving) {
                 vitesseHorizontal=0;
                 vitesseHorizontal=0;
                }
            
        
          
    }

    private void refreshVitesseWithDuréeChute(float tempsChute) {

 
            if (falling &&tempsChute > 0) {


                float vitesseTmp = gravité * tempsChute;



                if (vitesseTmp> vitesseMaxGravité*2){
                    vitesseTmp= vitesseMaxGravité;}
                vitesseVerticlaAccélération = vitesseTmp;
             } else {
                vitesseVertical = 0;
                vitesseVerticlaAccélération = 0;
            }
        
    }
    /*//////////////////////////Accesseur et mutateur simple///////////////////////*/
    public float getVitesseMax() {
        return vitesseMaxHorizontal;
    }

    public void setVitesseMax(float vitesseMax) {
        this.vitesseMaxHorizontal = vitesseMax;
    }

    public int getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(int acceleration) {
        this.acceleration = acceleration;
    }

    public int getDecceleration() {
        return decceleration;
    }

    public void setDecceleration(int decceleration) {
        this.decceleration = decceleration;
    }

    public float getGravité() {
        return gravité;
    }

    public void setGravité(float gravité) {
        this.gravité = gravité;
    }



      /*//////////////////////////Méthode static///////////////////////*/
  
    /**
     * permet de définit l'endroit ou sera l'objet selon le point de départ, le déplacement et le temps de l'application qu'il sera
     * @param pointDeDépart l'endroit de départ de l'objet
     * @param etat donnée sur le déplacement
     * @param durée du déplacement en ms
     * @return un point ou l'objet sera dans telle condition
     */

    public static FloatPoint getPositionOfThis(FloatPoint pointDeDépart, EtatDeplacement etat, int dansXMilliSeconde )
    {

        FloatPoint p = new FloatPoint();
        p.x = pointDeDépart.x;
        p.y = pointDeDépart.y;

        float d = ((float)dansXMilliSeconde)*((float)etat.getVitesseHorizontalAvecAccélération())/(float)1000.0 ;
        p.x += d;
         d = ((float)dansXMilliSeconde)*((float)etat.getVitesseVerticalAvecAccélération())/(float)1000.0 ;
       p.y += d;
        return p;



    }

    public void setVitesseMin(int i) {
        vitesseMinHorizontal =i;
    }
}

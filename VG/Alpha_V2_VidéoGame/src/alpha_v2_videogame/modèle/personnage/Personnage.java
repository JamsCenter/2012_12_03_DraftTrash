/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package alpha_v2_videogame.modèle.personnage;

import alpha_v2_videogame.modèle.ElementMovible;
import java.awt.Image;
import alpha_v2_vidéogame.mécanisme.SérieDeMéthodeStatic;

/**
 *
 * @author Fabrice
 */
public class Personnage extends ElementMovible {

    private int amure;
    private int attaque;
    /** possèdes toutes caractéristiques sur le déplacement du personnage*/
    private boolean invicilité;
    private int vieActuel;
    private int vieMax;

    /**
     * 
     * @param nom
     * @param largeur
     * @param hauteur
     * @param images
     * @param solide 
     */
    public Personnage(String nom, float largeur, float hauteur, Image[] images, boolean solide) {
        super(nom, largeur, hauteur, images, solide);

       
    }

  

    public int getAmure() {
        return amure;
    }

    public void setAmure(int amure) {
        this.amure = amure;
    }


    public void setLargeur(float largeur) {
        this.largeur = largeur;
    }


    public void setHauteur(float hauteur) {
        this.hauteur = hauteur;
    }

    public int getAttaque() {
        return attaque;
    }

    public void setAttaque(int attaque) {
        this.attaque = attaque;
    }



    public boolean isInvicilité() {
        return invicilité;
    }

    public void setInvicilité(boolean invicilité) {
        this.invicilité = invicilité;
    }

    public int getVieActuel() {
        return vieActuel;
    }

    public void setVieActuel(int vieActuel) {
        this.vieActuel = vieActuel;
    }

    public int getVieMax() {
        return vieMax;
    }

    public void setVieMax(int vieMax) {
        this.vieMax = vieMax;
    }


    /**se charge de recolté les viteses bonus de ce personnage*/
    public float getVitesseBonus() {
        return 0;

    }

///////////////////////////////imagerie start///////////////////
    @Override
    public Image getImage(int i) {
        if (images == null || i < 0 || i > images.length) {
            return null;
        }
        return images[i];

    }

    @Override
    public Image getImageAnimation(long i) {
        return animations[0].getGoodImage(1000);
      //  return animations[0].getGoodImage(1000);

    }

    @Override
    public Image getCurrentImage() {

        String etat = "";
        Image image;
        if (isHeMoving()) {
            etat = "M ";
            image = getImageAnimation(SérieDeMéthodeStatic.getTimeNow());
            etat += "l";
            if (getVitesseHorizontal() > 0) {
              
                etat += "r";
            }
        } else {
            etat = "DM ";
            image = getImage(0);

        }

        if (isHeFalling()) {
            etat = "F ";

        }

        return image;

    }


///////////////////////////////imagerie start///////////////////
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package alphavideogame.moteur;

import alphavideogame.controleur.ControleurPrincipal;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

/**
 *
 * @author Eloi & Fab
 */
public class TimerApp implements ActionListener {

    /** Représente le temps entre deux évènements*/
    private long timeWhenStart;
    private long timeSinceStart;
    private int unitéTempsEnMS;
    private Timer timer;
    private ControleurPrincipal controlPrincipal;

    public TimerApp(int unitéTempsEnMS,ControleurPrincipal controleurAbonnéeAuTimer)
    {
        timeWhenStart=-1;
        timeSinceStart=0;
        this.unitéTempsEnMS = unitéTempsEnMS;
        controlPrincipal= controleurAbonnéeAuTimer;

        timer = new Timer(unitéTempsEnMS, this );
        // Could be use
        //timer.setActionCommand("TIMEEVENT");

        timer.start();




    }

    /** Evènement activé  à intervale propre à  unitéTempsEnMS donné à timer*/
    private int compteurSeconde =0;
    public void actionPerformed(ActionEvent e) {
    if(timeWhenStart<0){ timeWhenStart = e.getWhen();}
     timeSinceStart = e.getWhen();

     controlPrincipal.signalDeRafraichissement(e);


    timeSinceStart = e.getWhen();
    if (compteurSeconde <( e.getWhen()-timeWhenStart)/1000.0){
        compteurSeconde++;
    System.out.println("Tic "+compteurSeconde+" ("+(getTimeSinceStartApp())+")");
    }




    }

    public int getUnitéDeTemps(){return unitéTempsEnMS; }

    public long getTimeSinceStartApp() {
            return timeSinceStart -timeWhenStart ;
    }



}
